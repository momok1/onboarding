"""Downloader file"""
import os

from pytube import YouTube, exceptions


class Downloader:
    """Downloader class"""

    def __init__(self, url, path):
        self.url = url
        self.path = path
        self.you_tube = None
        self.yt_stream = None

    def verify_url(self):
        """Checking url by converting to YouTube element that shuld trow excaption
        if url is not correct"""
        try:
            self.you_tube = YouTube(self.url)
        except exceptions.PytubeError:
            print("Provided URL address can not be reached.")
            return False
        return True

    def is_download_path_exist(self):
        """Checking does download path exist"""
        return os.path.exists(os.path.abspath(self.path))

    def get_stream(self, audio_video: str):
        """Returning audio or video stream instance depandes what user pick"""
        if audio_video.lower() == "audio":
            self.yt_stream = self.you_tube.streams.get_audio_only()
            return self.yt_stream
        elif audio_video.lower() == "video":
            self.yt_stream = self.you_tube.streams.get_highest_resolution()
            return self.yt_stream
        else:
            return None

    def download(self):
        """YouTube should be downloaded on call of this funkcion"""
        if self.yt_stream is None:
            return False
        if self.is_download_path_exist() is False:
            return False
        try:
            print(
                "*-----*"
                + self.you_tube.title
                + "*-----*\n"
                + "*-----*Downloading*-----*"
            )
            self.yt_stream.download(self.path)
        except exceptions.PytubeError:
            print("Error: downloading file")
            return False
        print("*-----*Downloaded*-----*")
        return True
