# Feature: Youtube Downloader

## Scenario_1:

**Given**: one would like to download video from the Youtube to listen music offline. <br>
**When**: user provides URL of youtube video and download path. <br>
**Then**: video should be downloaded to provided download path. <br>
