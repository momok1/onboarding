"""Test file for downloader"""


import unittest
from downloader import Downloader
from pytube import Stream


class TestDownloader(unittest.TestCase):
    """Test Class"""

    def setUp(self):
        self.yt_downloader = Downloader(
            "https://www.youtube.com/watch?v=A8sFkwwf8Xk&ab_channel=BajagaiInstruktori",
            "TDD/TddExamples/YouTubeDownloader",
        )

    def test_should_return_true_when_url_is_correct(self):
        """Verifying URL adress"""
        self.assertTrue(self.yt_downloader.verify_url())

    def test_should_return_false_when_url_is_not_correct(self):
        """Verifying URL adress"""
        yt_downloader = Downloader(
            "Incorrect Youtube URL",
            "/home/pop/Downloads",
        )
        self.assertFalse(yt_downloader.verify_url())

    def test_should_return_true_when_download_path_existing(self):
        """Testing existing path"""
        self.assertTrue(self.yt_downloader.is_download_path_exist())

    def test_should_return_false_when_download_path_not_existing(self):
        """Testing non existed path"""
        yt_downloader = Downloader(
            "https://www.youtube.com/watch?v=A8sFkwwf8Xk&ab_channel=BajagaiInstruktori",
            "Incorrect download path",
        )
        self.assertFalse(yt_downloader.is_download_path_exist())

    def test_should_make_youtube_streams_audio_only_instance_when_audio_was_picked(
        self,
    ):
        """Testing returning streams for some values that user can pick"""

        self.yt_downloader.verify_url()
        self.assertIsInstance(self.yt_downloader.get_stream("audio"), Stream)
        self.assertTrue(self.yt_downloader.get_stream("audio").includes_audio_track)

    def test_should_make_youtube_video_stream_instance_when_video_was_picked(
        self,
    ):
        """Testing returning streams for some values that user can pick"""
        self.yt_downloader.verify_url()
        self.assertIsInstance(self.yt_downloader.get_stream("video"), Stream)
        self.assertTrue(self.yt_downloader.get_stream("video").includes_video_track)

    def test_should_return_none_if_user_pick_something_else_then_audio_or_video(
        self,
    ):
        """Testing returning streams for some values that user can pick"""
        self.yt_downloader.verify_url()
        self.assertIsNone(self.yt_downloader.get_stream("something else"), Stream)

    def test_should_return_true_if_stream_is_downloaded(self):
        """Testing download of youtube video"""
        self.yt_downloader.verify_url()
        self.yt_downloader.get_stream("audio")
        self.assertTrue(self.yt_downloader.download())

    def test_shuld_return_false_if_downloading_of_stream_failed(self):
        """Testing download of youtube video"""
        self.yt_downloader.verify_url()
        # do not make yt stream to make download fail
        self.assertFalse(self.yt_downloader.download())


unittest.main()
