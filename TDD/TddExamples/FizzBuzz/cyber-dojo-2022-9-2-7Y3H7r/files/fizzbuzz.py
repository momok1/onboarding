"""FizzBuzz"""


class FizzBuzz:
    """Class for the FizzBuzz"""

    def fizzbuzz(self, number):
        """
        Result is either a number, or Fizz if number is divisable with 3,
        or Buzz if number is divisable with 5
        """
        result = ""
        if (number % 3) == 0:
            result = "Fizz"
        if (number % 5) == 0:
            result += "Buzz"
        if result == "":
            result = str(number)
        return result
