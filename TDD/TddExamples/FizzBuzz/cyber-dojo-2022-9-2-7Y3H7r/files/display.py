"""Displaying numbers or Fizz/Buzz"""

from fizzbuzz import FizzBuzz


class Display:
    """Class for displaying numbers or Fizz/Buzz"""

    def show_numbers(self, count):
        """Iterate through the range of numbers and print numbers or Fizz/Buzz"""
        for i in range(count):
            print(FizzBuzz.fizzbuzz(self, i + 1))
