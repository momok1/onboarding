"""Testing FizzBuzz"""

import unittest
from fizzbuzz import FizzBuzz
from display import Display


class TestFizzBuzz(unittest.TestCase):
    """Class for testing FizzBuzz"""

    def test_should_return_one_when_number_is_one(self):
        self.assertEqual("1", FizzBuzz.fizzbuzz(self, 1))

    def test_should_return_fizz_when_number_is_three(self):
        self.assertEqual("Fizz", FizzBuzz.fizzbuzz(self, 3))

    def test_should_return_fizz_when_number_is_nine(self):
        self.assertEqual("Fizz", FizzBuzz.fizzbuzz(self, 9))

    def test_should_return_buzz_when_number_is_five(self):
        self.assertEqual("Buzz", FizzBuzz.fizzbuzz(self, 5))

    def test_should_return_buzz_when_number_is_twenty(self):
        self.assertEqual("Buzz", FizzBuzz.fizzbuzz(self, 20))

    def test_should_return_fizzbuzz_when_number_is_fifteen(self):
        self.assertEqual("FizzBuzz", FizzBuzz.fizzbuzz(self, 15))

    def test_should_return_fizzbuzz_when_number_is_sixty(self):
        self.assertEqual("FizzBuzz", FizzBuzz.fizzbuzz(self, 60))

    def test_fizzbuzz(self):
        Display.show_numbers(self, 100)


if __name__ == "__main__":
    unittest.main()  # pragma: no cover
