# Feature: Custom list of numbers

## Scenario_1:

**Given**: a list of even numbers in particular range is needed <br>
**When**: user provides a range <br>
**Then**: a list of even numbers in provided range should be generated <br>

## Scenario_2:

**Given**: a list of odd numbers in particular range is needed <br>
**When**: user provides a range <br>
**Then**: a list of odd numbers in provided range should be generated <br>