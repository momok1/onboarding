"""
Testing list of numbers
"""

import unittest
from list_of_numbers import numbers_list


class TestScenario1(unittest.TestCase):
    """
    Testing Scenario 1
    """

    def test_should_return_two(self):
        """Testing single even value"""
        numbers = [2]
        self.assertEqual(str(numbers), str(numbers_list(range(2, 3, 2))))

    def test_should_return_two_four_and_six(self):
        """Testing list of even values"""
        numbers = [2, 4, 6]
        self.assertEqual(str(numbers), str(numbers_list(range(2, 7, 2))))


class TestScenario2(unittest.TestCase):
    """
    Testing Scenario 2
    """

    def test_should_return_one(self):
        """Testing single odd value"""
        numbers = [1]
        self.assertEqual(str(numbers), str(numbers_list(range(1, 2, 2))))

    def test_should_return_one_three_and_five(self):
        """Testing list of odd values"""
        numbers = [1, 3, 5]
        self.assertEqual(str(numbers), str(numbers_list(range(1, 6, 2))))


if __name__ == "__main__":
    unittest.main()  # pragma: no cover
